<?php

// Flatten an array of data with full-path string keys
function flat($array, $separator = '|', $prefix = '', $flattenNumericKeys = false) {
    $result = [];

    foreach($array as $key => $value) {
        $new_key = $prefix . (empty($prefix) ? '' : $separator) . $key;

        // Make sure value isn't empty
        if(is_array($value)) {
            if(empty($value)) $value = null;
            else if(count($value) == 1 && isset($value[0]) && is_string($value[0]) && empty(trim($value[0]))) $value = null;
        }

        $hasStringKeys = is_array($value) && count(array_filter(array_keys($value), 'is_string')) > 0;
        if(is_array($value) && ($hasStringKeys || $flattenNumericKeys)) $result = array_merge($result, flat($value, $separator, $new_key, $flattenNumericKeys));
        else $result[$new_key] = $value;
    }

    return $result;
}